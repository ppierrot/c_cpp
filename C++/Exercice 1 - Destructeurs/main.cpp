#include <iostream>
#include <math.h>

using namespace std;

class point
{
    private :

        float x,y;
        float r,t; //redondants mais utiles.
        void majrt();

    public:

        void init(float,float);
        void affiche();
        //Constructeurs
        point(float,float);
        point(float);
        point();
        //Destructeurs
        ~point();
        void deplace(float,float);
        void homothetie(float);
        void rotation(float);
};

void point::affiche()
{
    cout << "x = " << x << endl;
    cout << "y = " << y << endl;
    cout << "r = " << r << endl;
    cout << "t = " << t*180/M_PI << endl;
}

point::point(float px,float py)
{
    x=px;
    y=py;
    cout << "Constructeur 1" << endl;
    majrt();
}

point::point(float p)
{
    x=p;
    y=p;
    cout << "Constructeur 2" << endl;
    majrt();
}

point::point()
{
    x=0;
    y=0;
    cout << "Constructeur 3" << endl;
    majrt();
}

point::~point()
{
    cout << "Destructeur" << endl;
}

void f() //Ceci est pour rajouter un destructeur immédiatement après un constructeur
{
    point tmp(99);// Attention, ne pas mettre de parenthèses si il n'y a pas de paramètres.
    cout << " Sortie fct " << endl;
}

void point::init(float px,float py)
    {
        x=px;
        y=py;
        majrt();
    }

void point::deplace(float dx,float dy)
    {
        x=x+dx;
        y=y+dy;
        majrt();
    }

void point::homothetie(float cf)
    {
        x=x*cf;
        y=y*cf;
        majrt();
    }

void point::majrt()
    {
        r=sqrt(x*x + y*y);
        t=atan2(x,y);
    }

void point::rotation(float alpha)
    {
     alpha=alpha*M_PI/180;
     t=t+alpha;
     x=r*cos(t);
     y=r*sin(t);
    }


int main()
{
    point a(10,10);
    a.affiche();
    a.init(2,3);
    a.deplace(8,7);
    a.rotation(90);
    a.affiche();
    point b(3);
    b.affiche();
    f();// execution de la fonction f.
    point c;
    c.affiche();
}
