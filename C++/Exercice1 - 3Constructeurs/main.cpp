#include <iostream>

using namespace std;

class point
{
    private :

        float x,y;

    public:

        void init(float,float);
        void affiche();
        //Constructeurs
        point(float,float);
        point(float);
        point();
};

void point::affiche()
{
    cout << "x = " << x << endl;
    cout << "y = " << y << endl;
}

point::point(float px,float py)
{
    x=px;
    y=py;
    cout << "Constructeur 1" << endl;
}

point::point(float p)
{
    x=p;
    y=p;
    cout << "Constructeur 2" << endl;
}

point::point()
{
    x=0;
    y=0;
    cout << "Constructeur 3" << endl;
}

int main()
{
    point a(10,20);
    a.affiche();
    point b(3);
    b.affiche();
    point c;
    c.affiche();
}
