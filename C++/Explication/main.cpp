#include <iostream>

using namespace std;

class point
{
    //Tout ce qui suit est public (accessible depuis l'ext�rieur)
    public:

    void init(float,float);

    void affiche();






    //Tout ce qui suit est priv� (inaccessible depuis l'ext�rieur)
    private:

    float x,y;
};

// C'est la fonction de la classe en elle-m�me, le "point::" correspond au nom de la classe et les "::" correspondent au fait que la fonction init appartient a la classe point.
void point::init(float px,float py)
{
    x=px;
    y=py;
}

void point::affiche()
{
    //endl = "\n"
    cout <<"x = " << x << endl;
    cout <<"y = " << y << endl;
}

int main()
{
    point a,b;
    a.init(1,2);
    b.init(13,14);
    a.affiche();
    b.affiche();
}
