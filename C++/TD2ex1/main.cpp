#include <iostream>

using namespace std;

class vecteur2
{
    protected :
        int *tab;
        int nb;

    public :
        void affiche();
        vecteur2(int);
        ~vecteur2();
        vecteur2(int,int[]);
        void init();
        void homothetie(int);
        int scalaire(vecteur2);
};

vecteur2::vecteur2(int pnb)
{
    nb=pnb;
    tab = new int [nb];
    for(int i=0;i<nb;i++)
        tab[i]=0;
}

vecteur2::~vecteur2()
{
    //rien � prendre en charge
    delete tab;
    cout << "destructeur" << endl;
}

vecteur2::vecteur2(int pnb,int ptab[])
{
    nb=pnb;
    tab = new int [nb];
    for(int i=0;i<nb;i++)
        tab[i]=ptab[i];
}

void vecteur2::affiche()
{   cout <<"affichage" << endl;
    for(int i=0;i<nb;i++)
        cout << tab[i] << endl;
}

void vecteur2::init()
{
    for(int i=0;i<nb;i++)
    {
        cout << "entrer valeur" << endl;
        cin >> tab[i];
    }
}

void vecteur2::homothetie(int cf)
{
    for (int i=0;i<nb;i++)
        tab[i]=tab[i]*cf;
}
int vecteur2::scalaire(vecteur2 pv)
{
    int s;
    if (nb==pv.nb)
    {
        s=0;
        for (int i=0;i<nb;i++)
           s=s+tab[i]*pv.tab[i];
    }
    else {cout << "erreur " << endl; s=0;}
    return s;
}

class matrice : public vecteur2
{
    protected :
        int nbl,nbc;

    public :
        matrice(int,int);
        matrice(int,int,int*);
        ~matrice();
        void affiche();
        matrice(const matrice&);
};

matrice::matrice(int pnbl, int pnbc): vecteur2(pnbl * pnbc) // Multipli� le nombre de colonnes par le nombre de lignes pour obtenir le nombre de cases.
{
    nbl=pnbl;
    nbc=pnbc;
}

matrice::matrice(int pnbl,int pnbc,int *ptab) : vecteur2(pnbl * pnbc, ptab) // Pareil qu'au dessus mais on remplis les cases avec les donn�es de ptab.
{
    nbl=pnbl;
    nbc=pnbc;
}

void matrice::affiche()
{
    //vecteur2::affiche();
    int i,j;

    for(i=0;i<nbl;i++)
        for(j=0;j<nbc;j++)
            cout << tab[i*nbc+j] << "\t";
        cout << endl;
}

matrice::~matrice()
{
    // On n'a rien a rajouter car tout est d�j� pris en compte par le destructeur de la classe vecteur2
    cout << "destructeur de la matrice" << endl;
}

int main()
{
    int tmp[]={2,4,6,8,10,12};
    matrice m1(2,3);
    m1.affiche();
    matrice m2(2,3,tmp);
    m2.affiche();
    matrice m3(3,2,tmp);
    m3.affiche();
    //matrice m4(m2);
    //m4.affiche();
}
