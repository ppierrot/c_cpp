#include <iostream>
#include <math.h>

using namespace std;

class point
{
    private :

        float x,y;
        float r,t; //redondants mais utiles.
        void majrt();

    public:

        void init(float,float);
        void affiche();
        //Constructeurs
        point(float,float);
        point(float);
        point();
        //Destructeurs
        ~point();
        void deplace(float,float);
        void homothetie(float);
        void rotation(float);
};

void point::affiche()
{
    cout << "x = " << x << endl;
    cout << "y = " << y << endl;
    cout << "r = " << r << endl;
    cout << "t = " << t*180/M_PI << endl;
}

point::point(float px,float py)
{
    x=px;
    y=py;
    cout << "Constructeur 1" << endl;
    majrt();
}

point::point(float p)
{
    x=p;
    y=p;
    cout << "Constructeur 2" << endl;
    majrt();
}

point::point()
{
    x=0;
    y=0;
    cout << "Constructeur 3" << endl;
    majrt();
}

point::~point()
{
    cout << "Destructeur" << endl;
}

void f() //Ceci est pour rajouter un destructeur imm�diatement apr�s un constructeur
{
    point tmp(99);// Attention, ne pas mettre de parenth�ses si il n'y a pas de param�tres.
    cout << " Sortie fct " << endl;
}

void point::init(float px,float py)
    {
        x=px;
        y=py;
        majrt();
    }

void point::deplace(float dx,float dy)
    {
        x=x+dx;
        y=y+dy;
        majrt();
    }

void point::homothetie(float cf)
    {
        x=x*cf;
        y=y*cf;
        majrt();
    }

void point::majrt()
    {
        r=sqrt(x*x + y*y);
        t=atan2(x,y);
    }

void point::rotation(float alpha)
    {
     alpha=alpha*M_PI/180;
     t=t+alpha;
     x=r*cos(t);
     y=r*sin(t);
    }

class rectangle
{
    protected :

        point p1;
        point p2;

    public :

        rectangle(int,int,int,int);
        rectangle(point,point);
        rectangle(point);
        ~rectangle();
        void affiche();
};

rectangle::rectangle(int x1,int y1,int x2 ,int y2):p1(x1,y1),p2(x2,y2)
{
    //rien � prendre en charge, tout est pris en charge dans la classe point
}

rectangle::rectangle(point p1,point p2):p1(p1),p2(p2)
{
    //rien � prendre en charge, tout est pris en charge dans la classe point
}

rectangle::rectangle(point p1):p1(p1),p2(p2)
{
    p2.deplace(20,40);
}

//rectangle::rectangle(const rectangle&) : p1 &src
//{

//}

//rectangle& operator=(const rectangle&);
//{

//}

rectangle::~rectangle()
{

}

void rectangle::affiche()
{
    cout << "les coordonnees des points sont:" << endl;
    p1.affiche();
    cout << "les coordonnees des points sont:" << endl;
    p2.affiche();
}

//class bouton
//{
//   protected :
//        retangle r;
//        string ch;

//    public :



//};

class menu1
{
    protected :

    class bouton *tab;
    int n;

    public :

    menu1(char*[], int);
    ~menu1();
    void affiche();

    menu1::menu1(char* tmp[], int nb)
    {
        n = nb;
        tab = new bouton [n]; //appel du constructeur par d�faut -> bouton ""
    }

    menu1::~menu1()
    {
        delete []tab;
    }

    void menu1::affiche()
    {
        fr(int i = 0, i<x, i++)
        tab[i].affiche();
    }


};

class menu2
{
    protected :
    bouton** tab;
    int n;

    public :

    menu2(char*[], int);
    ~menu2();
    void affiche();

    menu2::menu2 (char* tmp[], int nb)
    {
        n=nb;
        tab=new bouton* [n];
        for (int i=0;i<n;i++)
            tab[i] = new bouton(60, 70*(i+1)),120, 20+(i+1)*70,tmp[i];
    }

    menu2::~menu2()
    {
        for (int i=0;i<n;i++)
            delete (tab[i]);
        delete tab;
    }

    void menu2::affiche()
    {
        for (int i=0;i<n;i++)
            tab[i]->affiche(); //-> : �a veut dire que c'est une �galit� de l'adresse et pas de la valeur (quand on utilise =)
    }
};

class menu3
{
    protected :

    vector <bouton> tab;
    int n;

    public :

    menu3(char*[], int);
    menu3();
    void.affiche();

    menu3::menu3(char* tmp[], int nb)
    {
        n=nb;
        for (int i=0;i<n;i++)
        {
            bouton*btmp new bouton (60,70*(i+1),120,20+(i+1)*70,tmp[i]);
            tab.push_back(*btmp);
            delete btmp;
        }
    }

    menu3::~menu3()
    {

    }

    void menu3::affiche()
    {
        for (int i=0;i<n;i++)
            tab[i].affiche();

    }
};


int main()
{
    point pp1(10,20);
    point pp2(30,40);
    rectangle r1(10,20,30,40);
    r1.affiche();
    rectangle r2(pp1,pp2);
    r2.affiche();
    rectangle r3(pp1);
    r3.affiche();
    class *tmp[]={"label1","label2"};
    menu1 m1 (tmp, 3);
    m1.affiche;
    //rectangle r4(r2);
    //r4.affiche();
    //r3=r4;
    //r3.affiche();
};
