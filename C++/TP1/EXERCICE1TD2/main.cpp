#include <iostream>
#include <string.h>

using namespace std;

class Chaine
{
    private:
        char *tab; // CREATION DU TABLEAU POUR STOCKER LA CHAINE AVEC TOUJOURS UNE CASE EN PLUS AVEC UN 0 POUR POUVOIR CONSIDERER LE TABLEAU COMME UNE CHAINE (DELIMITEUR)//
        int nb; // CREATION DE LA CASE POUR STOCKER NB
        int *stat; // OU int stat[26]; // CREATION DU TABLEAU STAT
        void majstat(); // FONCTION MISE A JOUR DU TABLEAU STAT

    public:
        Chaine(); //CONSTRUCTEUR DE BASE (VIDE)
        Chaine(char*); //CONSTRUCTEUR POUR TMP VOIR PLUS BAS CHAINE = "abbac"//
        Chaine(const Chaine&); // CONSTRUCTEUR DE RECOPIE => CLONAGE
        ~Chaine(); // DESTRUCTEUR
        void affiche(); //FONCTION AFFICHE
        char *Gettab(); //ACCESSEUR POUR TAB
        int Getnb(); //ACCESSEUR POUR NB
};

Chaine::Chaine() //CONSTRUCTEUR DE BASE (VIDE)//
{
    tab=NULL;
    nb=0;
    stat = new int [26];
    majstat();
}

Chaine::Chaine(char *ptab) //CONSTRUCTEUR POUR TMP VOIR PLUS BAS//
{
    nb=strlen(ptab);//comptage caract�res dans tab
    tab = new char [nb+1];//instanciation du tableau de caract�res "tab"
    tab = strcpy(tab,ptab); // copier la chaine (tab est la destination et ptab est la source)
    stat = new int [26]; // cr�ation du tableau stat avec 26 cases pour les 26 caract�res de l'alphabet
    majstat(); //utilisation de la fonction mise a jour//
}

void Chaine::affiche() //FONCTION AFFICHE //
{
    if (tab!=NULL) //TOUT CE QUI EST EN DESSOUS NE MARCHE QUE SI TAB N'EST PAS NULL
    {
        cout << "afficher la chaine " << tab << endl; //AFFICHAGE
        cout << "afficher nb = " << nb << endl; //AFFICHAGE
        cout << "afficher le contenu de stat" << endl; //AFFICHAGE
    for (int i=0;i<26;i++) //BOUCLE DE 0 A 26 pour afficher toutes les donn�es de stat
        cout << stat[i]<< endl; //AFFICHAGE DU TABLEAU STAT de 0 a 26
    for (int i=0;i<26;i++) // BOUCLE DE 0 A 26 POUR AFFICHER "Il y a NB fois le caract�re (a,b,c ou d etc...)
        if(stat[i]!=0)
            cout << "il y a " << stat[i] << " " << (char)(i+'a') << endl;
    }
}

Chaine::~Chaine() //DESTRUCTEUR
{
    delete tab; //DESTRUCTION DU TABLEAU TAB A LA FIN
    delete stat; //DESTRUCTION DU TABLEAU STAT A LA FIN
    cout << "destructeur " << endl; //AFFICHER LE MESSAGE QUI DIT QUE SA A BIEN ETE DETRUIT
}

void Chaine::majstat() //MISE A JOUR DU TABLEAU STAT//
{
    char car; // DECLARATION DE LA VARIABLE CAR EN TYPE LETTRE
    int indice; //DECLARATION DE LA VARIABLE INDICE EN TYPE ENTIER
    for(int i=0;i<26;i++) //BOUCLE DE 0 A 26 POUR INITIALISER TOUTES LES CASES A 0
        {
            stat[i]=0;
        }
    for(int i=0;i<nb;i++) // BOUCLE DE 0 A NB POUR PARCOURIR LES 26 CARACTERES DE L'ALPHABET
    {
        car = tolower(tab[i]); // LA VARIABLE CAR TRANSFORME TOUS LES CARACTERES DU TABLEAU TAB EN MINUSCULES.//
        if((car>='a')&&(car<='z')) // DELIMITER LE CALCUL DE L'INDICE UNIQUEMENT SUR LES LETTRES DE L'ALPHABET ET IGNORER LES CARACTERES SPECIAUX
        {
            indice = car - 'a'; // CODE ASCII DU CARACTERE - CODE ASCII DU CARACTERE A
            stat[indice]++; // ON INCREMENTE LE TABLEAU STAT AVEC LE NOMBRE QU'ON OBTIENT DANS INDICE
        }
    }
}

Chaine::Chaine(const Chaine &src) //CONSTRUCTEUR DE RECOPIE
{
    nb=src.nb;
    tab = new char [nb+1];
    stat = new int [26];
    for(int i=0;i<=nb;i++)
        {
            tab[i]=src.tab[i];
        }
    for(int i=0;i<26;i++)
        {
            stat[i]=src.stat[i];
        }
    cout << "constructeur de recopie" << endl;
}


char* Chaine::Gettab() //ACCESSEUR POUR TAB//
{
    return tab;
}

int Chaine::Getnb() // ACCESSEUR POUR NB//
{
    return nb;
}

int main()
{
    //Chaine S1;
    //S1.affiche();
    char tmp[]="abbac";
    Chaine S2(tmp);
    S2.affiche();
    Chaine S1(S2); //recopie de S2 dans S1
    S1.affiche();
    cout << "chaine = " <<S1.Gettab() << endl; //AFFICHAGE ACCESSEUR TAB//
    cout << "nb = " <<S1.Getnb() << endl; //AFFICHAGE ACCESSEUR NB//
    //Chaine S3("iut");
    //S3.affiche();
}
