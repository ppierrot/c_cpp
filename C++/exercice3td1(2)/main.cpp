#include <iostream>

using namespace std;

class vecteur2
{
    private :
        int *tab;
        int nb;

    public :
        void affiche();
        vecteur2(int);
        ~vecteur2();
        vecteur2(int,int[]);
        void init();
        void homothetie(int);
        int scalaire(vecteur2);
};

vecteur2::vecteur2(int pnb)
{
    nb=pnb;
    tab = new int [nb];
    for(int i=0;i<nb;i++)
        tab[i]=0;
}

vecteur2::~vecteur2()
{
    //rien � prendre en charge
    delete tab;
    cout << "destructeur" << endl;
}

vecteur2::vecteur2(int pnb,int ptab[])
{
    nb=pnb;
    tab = new int [nb];
    for(int i=0;i<nb;i++)
        tab[i]=ptab[i];
}

void vecteur2::affiche()
{   cout <<"affichage" << endl;
    for(int i=0;i<nb;i++)
        cout << tab[i] << endl;
}

void vecteur2::init()
{
    for(int i=0;i<nb;i++)
    {
        cout << "entrer valeur" << endl;
        cin >> tab[i];
    }
}

void vecteur2::homothetie(int cf)
{
    for (int i=0;i<nb;i++)
        tab[i]=tab[i]*cf;
}
int vecteur2::scalaire(vecteur2 pv)
{
    int s;
    if (nb==pv.nb)
    {
        s=0;
        for (int i=0;i<nb;i++)
           s=s+tab[i]*pv.tab[i];
    }
    else {cout << "erreur " << endl; s=0;}
    return s;
}


int main()
{
    // vecteur2 a(4);
//    a.affiche();
    int tmp[]={1,2,3,4};
    vecteur2 b(4,tmp);
    vecteur2 a(4,tmp);
    a.affiche();
    b.init();
    b.homothetie(2);
    b.affiche();
    int ps;
    ps=a.scalaire(b);
    cout << "ps =" << ps << endl;
}
