#include "Arme.h"

using namespace std;

Arme::Arme() //arme de base
{
    a_nomArme = "Ep�e";
    a_degatsArme = 15;

}

Arme::Arme(std::string nom, int degats) : a_nomArme(nom), a_degatsArme(degats)
{

}

int Arme::getDegats() const //Accesseur qui permettra exclusivement de r�cup�rer la valeur des d�g�ts contenuent dans l'attribut de la classe Arme, afin de faire fonctionner la m�thode attaque dans la classe Personnage.
{
    return a_degats;
}

void changer(std::string nom, int degats);
{
    a_nomArme=nom;
    a_degatsArme=degats;
}

void afficher() const; // const = vous indiquez au compilateur que votre m�thode ne modifie pas l'objet, c'est-�-dire qu'elle ne modifie la valeur d'aucun de ses attributs
{
    cout << "Arme : " <<a_nomArme<< " �quip�e, provoque "<<a_degatsArme<< " de dommages"<<endl; //affichage pour indiquer les d�g�ts caus�s
}
