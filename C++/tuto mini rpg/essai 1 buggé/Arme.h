#include <string>

class Arme
{
    public : //constructeurs + m�thodes (void)
    Arme();
    Arme(std::string nom, int degats);
    void changer(std::string nom, int degats);
    void afficher() const;

    private ://attributs
    std::string a_nomArme;
    int a_degatsArme;
    int getDegats() const;
};
