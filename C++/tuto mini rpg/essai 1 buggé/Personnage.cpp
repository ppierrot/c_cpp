#include "Personnage.h"
using namespace std;

Personnage::Personnage()//constructeur d�faut
{
    a_vie = 100;
    a_arme = "Ep�e";
    a_degats = 15;
}

Personnage::Personnage(string autre_arme, int autre_degats) : vie (100), a_arme(autre_arme), a_degats(autre_degats)//constructeur avec meilleure arme
{

}

Personnage::Personnage(int autre_vie) : a_vie (autre_vie) //constructeur pour plus de vie de d�part
{

}

Personnage::~Personnage() //destructeur
{

}

void Personnage::degats_recu(int nb_degats)
{
    a_vie -= nb_degats;

    if (vie<0)
    {
        vie=0;
    }
}


void Personnage::attaque(Personnage &cible)
{
    cible.degats_recu(a_arme.getDegats());//getDegats renvoie le nombre de d�g�ts, qu'on envoi � la m�thode degats_recu.
}

void Personnage::consommer_potion(int heal_potion)
{
    a_vie+=heal_potion;

    if (a_vie>150)
    {
        a_vie = 150;
    }
}

void Personnage::chgmt_arme(string new_arme, int degats_new_arme)
{
    a_arme.changer(new_arme, degats_new_arme)
}

bool Personnage::vivants() const
{
    if (a_vie > 0)
    {
        return true;
    }
    else
    {
        return false;
    }
}


void Personnage::etat()const //affiche l'�tat du personnage concern� et son arme
{
    cout << "Vie = " <<a_vie <<endl;
    a_arme.afficher();
}
