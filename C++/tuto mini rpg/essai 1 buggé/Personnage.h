#ifndef PERSONNAGE_H_INCLUDED
#define PERSONNAGE_H_INCLUDED
#include <string>
#include "Arme.h"
class Personnage
{
    public :

    Personnage();//constructeur d�faut
    Personnage(std::string arme, int degats);//constructeur avec meilleure arme
    Personnage(int vie); //constructeur pour plus de vie de d�part
    ~Personnage(); //destructeur (sert � rien dans ce cas l� car c'est progrzmme statique)
    void degats_recu(int degats);//m�thode qui s'occupe des dommages inflig�s
    void attaque(Personnage &cible);//m�thode qui prend un objet et le fait attaquer un autre
    void consommer_potion(int heal_potion);//m�thode qui permet de redonner de la vie
    void chgmt_arme(std::string new_arme, int new_degats);//m�thode qui fait �quiper une nouvelle arme
    bool vivants();//m�thode qui fait revenir la vie � 0 si on passe dans le n�gatif
    void etat() const;//affiche l'�tat du personnage concern� et son arme

    private :

    int a_vie;//attribut
};

#endif // PERSONNAGE_H_INCLUDED
