#include <iostream>
#include <string>
#include "Personnage.h"

using namespace std;

class Personnage
{
    //m�thodes

    public :

    //on met void � une m�thode quand cette derni�re n'a pas de return

    void Personnage::recevoirDegats(int Degats) //un Personnage encaisse d�g�ts d'une attaque
    {
        m_vie -= nbDegats;
        //On enl�ve le nombre de d�g�ts re�us � la vie du personnage

        if (m_vie < 0) //Pour �viter d'avoir une vie n�gative
        {
            m_vie = 0; //On ramm�ne la vie � 0 si il devait passer dans les n�gatifs
        }
    }

    //les r�f�rences : En C++ on utilise un maximum les r�f�rences quand on doit passer un objet d'un endroit � un autre (typiquement en argument d'une fonction).

    void Personnage::attaquer(Personnage &cible) //entreprend d'attaquer un autre Personnage, on utilise une r�f�rence (&) qui prend en param�tre un objet Personnage
    {
        cible.recevoirDegats(m_degatsArme); //on appel la m�thode recevoirDegats en l'asso�iant au param�tre objet (un alias pour un objet Personnage) pour que l'attribut qui repr�sente les d�g�ats de l'arme de CE personnage (c'est pour �a qu'on encapsule recevoirDegats en faisant objet.m�thode et qu'on utilise pas directement ce dernier sinon il ne sait pas de quelle arme il faut infliger des d�g�ts)
    }

    void Personnage::potion (int quantitePotion) //un Personnage augmente la valeur de m_vie
    {
        m_vie += quantitePotion;
        //mettre condition qui limite aux pts de vie max, probablement faire un attribut pour pv max

    }

    void Personnage::changerArme(string nomNouvelleArme, int degatsNouvelleArme) //un Personnage modifie m_nomArme et donc m_degatsArme par la m�me occasion
    {

    }

    //on met bool � une m�thode quand cette derni�re return un bool�en (vrai ou faux)

    bool Personnage::estVivant() //on v�rifie si un Personnage a une valeur de m_vie au-dessus de 0
    {

    }

};
