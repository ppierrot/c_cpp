#ifndef DEF_PERSONNAGE
#define DEF_PERSONNAGE

#include <string>

class Personnage
{
    //prototypes

    public :

    void recevoirDegats(int Degats);
    void attaquer(Personnage &cible);
    void potion (int quantitePotion);
    void changerArme(string nomNouvelleArme, int degatsNouvelleArme);
    bool estVivant();


    //attributs

    private :

    int m_vie;
    std::string m_nomArme; //Pas de using namespace std, il faut donc mettrestd:: devant string
    int m_degatsArme;
};
