
int main ()
{
    Personnage Xena, Hercules; //Cr�ation de 2 objets de type Personnage

    Hercules.attaquer(Xena); //on appel la m�thode attaquer, Hercules attaque Xena
    Xena.potion(20); //on appel la m�thode potion et on apllique le montant de vie entre parenth�ses
    Xena.attaquer(Hercules); //on appel la m�thode attaquer, Xena attaque Hercules
    Hercules.attaquer(Xena); //on appel la m�thode attaquer, Hercules attaque Xena
    Xena.changerArme("Chakram", 40); //on appel la m�thode changerArme, Xena change d'arme : elle opte pour un Chakram qui fait 40 de d�g�ts
    Xena.attaquer(Hercules); //on appel la m�thode attaquer, Xena attaque Hercules


    return 0;
}
