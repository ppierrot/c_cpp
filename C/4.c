#include <stdio.h>
#include <stdlib.h>

int fonction1(int n) //fonction prenant en paramètre un entier n et renvoyant un entier 
{
    int som=1,i;
    for (i=1;i<=n;i++)
    {
        som=som*i;

    }
    return som;
}

int fonction2(int x, int y)
{
    int som=1,i;
    for (i=1;i<=y;i++)
    {
        som=x*x;
    }
    return som;
}

int fonction3(int n, int p)
{
    int res;
    res = fonction1(n)/(fonction1(n-p)*fonction1(p));
    return res;
}

int main()
{
    int rslt,nbr;
    scanf("%d", &nbr);
    rslt = fonction1(nbr); //appel de fonction 1 -> fonction1()
    printf("%d", rslt);
    int nbr2;
    scanf("%d", &nbr);
    scanf("%d", &nbr2);
    rslt = fonction2(nbr,nbr2);
    printf("%d", rslt);
    scanf("%d", &nbr);
    scanf("%d", &nbr2);
    rslt = fonction3(nbr,nbr2);
    printf("%d", &rslt);
    return 0;
}
