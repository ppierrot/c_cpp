#include <stdio.h>
#include <stdlib.h>
#include <math.h>

float fonction(int n)
{
    float som=0;
    int i;
    for (i=1;i<=n;i++)
    {
        som=som+1/pow(i,4); //pow -> puissance de quelque chose
    }
    float pi;
    pi=pow(90*som,0.25);
    return pi;
}

int main()
{
    int n;
    scanf("%d", &n);
    float rslt;
    rslt = fonction(n);
    printf("%f", rslt);
    return 0;
}
