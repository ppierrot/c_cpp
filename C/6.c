#include <stdio.h>
#include <stdlib.h>

float fct(float a)
{
    float xn1,xn;
    xn1=(1+a)/2;
    do
    {
        xn=xn1;
        xn1=0.5*(xn+a/xn);
    }
    while (fabs((xn1-xn)/xn)>0.0001); //fabs->  valeur numérique d'un nombre sans tenir compte de son signe. On peut la comprendre comme sa distance de zéro ; ou comme sa valeur quantitative.
    return xn1;
}

int main()
{
    float res,n;
    scanf("%f", &n);
    res=fct(n);
    printf("%f", res);
    return 0;
}
