#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#define pi 3.141592654
//Pastore Pierre groupe F
// !! -> En compilant le programme �a ne fonctionne pas correctement pour la raison suivante : en arrivant � la fonction "saisie_piquets", il va ignorer la premi�re boucle for, ne permettant pas de pouvoir rentrer les coordonnees. Je pense que c'est en rapport avec le tableau que je passe en param�tre de cette m�me fonction. J'ai �ssay� une m�thode en lui attribuant un pointeur et en le return � la fin de la fonction, ou encore de le passer en variable globale mais rien n'y fait malgr� mes efforts.?

//variables globales
int n;//nombre de piquets
int i;//variable d'iteration pour les boucles
float A = 0; //aire de la cloture
float Gxi = 0;//coordonnee x du centre de gravite
float Gyi = 0;//coordonnee y du centre de gravite

int nbr_piquets(int n) //on demande le nombre de piquets
{
    printf("Rentrez nombre de piquets \n");
    scanf("%d", &n);
    while (n<3 || n>50)
    {
      printf("veuillez rentrer un nombre de piquets compris entre 3 et 50 \n");
      scanf("%d", &n);
    }
    printf("nombre de piquets : %d \n", n);
    return n;
}

float saisie_piquets(float coordonnees[n][2],int n) //on donne les coordonnees � mettre dans le tableau
    {
        for (i=0;i<n-1;i++) //on rentre les coordonnees dans le tableau
        {
            float x,y;
            printf("piquet %d : veuillez rentrer l'absicce :", i);
            scanf("%f", &x);
            coordonnees[i][0] *= x;
            printf("piquet %d : veuillez rentrer l'ordonnee :", i);
            scanf("%f", &y);
            coordonnees[i][1] *= y;
        }
        printf("affichage coordonnees piquets : \n"); //on affiche le tableau pr�c�dent
        for (i=0;i<n-1;i++)
        {
            printf("%d - x : %f , y : %f \n", i, coordonnees[i][0], coordonnees[i][1] );
        }
        return coordonnees[n][2];

    }

float aire(float coordonnees[n][2])//calcul de l'aire de la cl�ture
{
    for (i=0;i<n-1;i++)
    {
            A += coordonnees[i][0] * (coordonnees[i+1][1]) - (coordonnees[i+1][0]) * coordonnees[i][1];
    }

    A += coordonnees[1][0] * (coordonnees[1+1][1]) - (coordonnees[1+1][0]) * coordonnees[1][1];//on boucle la cloture

    A /= 2; //on divise par 2 comme sp�cifi� dans la formule de la somme
    printf("aire cloture = %f \n", A);
}


float gravite_x(float A, float coordonnees[n][2])//calcul absicce x centre gravit� G
{
    for (i=0;i<n;i++)
    {
        Gxi += (coordonnees[i][0] + coordonnees[i+1][0]*coordonnees[i][0]) * ((coordonnees[i+1][1] - coordonnees[i+1][0]) * coordonnees[i][1]);
    }
    Gxi*=1/(6*A); //on divise par 6 fois l'aire de la cloture comme sp�cifi� dans la formule de la somme
    printf("Coordonne x centre gravite = %f \n", Gxi);

}

float gravite_y(float A, float coordonnees[n][2])//calcul absicce y centre gravit� G
{
    for (i=0;i<n;i++)
    {
        Gyi += (coordonnees[i][1] + coordonnees[i+1][1]*coordonnees[i][0]) * ((coordonnees[i+1][1] - coordonnees[i+1][0]) * coordonnees[i][1]);
    }
    Gyi*=1/(6*A); //on divise par 6 fois l'aire de la cloture comme sp�cifi� dans la formule de la somme
    printf("Coordonne y centre gravite = %f \n", Gyi);
}


float position(float coordonnees[n][2])//d�termination si centre de gravit� dans la cloture
{
    float theta_i,som=0;
    for (i=0;i<n-1;i++)
    {
        int ii = pow (i,2); //on fait le carr� de i afin de pouvoir rentrer en param�tre de tableau ce m�me carr�, dans le cacul suivant
        theta_i = acos(((coordonnees[i][0]*(coordonnees[i+1][0])) + (coordonnees[i][1]*coordonnees[i+1][1]))/sqrt ((coordonnees[ii][0]+coordonnees[ii][1])*(coordonnees[ii+1][0]+coordonnees[ii+1][1]))); //formule angle theta
        som += theta_i;
    }
    printf("som = %f \n", som);
    if (som=pi*2)
    {
        printf("La vache est dans la cloture \n");
    }
    else
    {
        printf("Attention, la vache est hors du pr� \n");
    }
}

int main()
{
    nbr_piquets(n);
    float coordonnees[n][2];//cr�ation tableau des coordonnees
    saisie_piquets(coordonnees,n);
    aire(coordonnees);
    gravite_x(A,coordonnees);
    gravite_y(A,coordonnees);
    position(coordonnees);

    return 0;
}
